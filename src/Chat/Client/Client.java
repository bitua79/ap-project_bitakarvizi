
package Chat.Client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client extends Application {

    public static DataInputStream inputStream;
    public static DataOutputStream outputStream;
    private String in;
    public Socket socket;
    private TextArea serverText;
    private TextArea clientText;

    @Override
    public void start(Stage stage) throws Exception {

        HBox hBox = new HBox();
        serverText = new TextArea();
        serverText.setEditable(false);
        serverText.appendText("Connecting...\n");
        clientText = new TextArea();

        Button send = new Button("send");
        send.setOnAction(e -> sendButtonClicked());

        hBox.getChildren().addAll(serverText, clientText);
        stage.setScene(new Scene(new VBox(hBox, send), 400, 400));
        stage.show();

        new Thread(() ->
        {
            try
            {
                Socket socket = new Socket("localhost", 8000);
                Platform.runLater(() -> clientText.appendText("Connected to Server!\n"));

                outputStream = new DataOutputStream(socket.getOutputStream());
                inputStream = new DataInputStream(socket.getInputStream());

                while(true)
                {
                    String text = inputStream.readUTF();
                    Platform.runLater(() -> serverText.appendText( text + "\n"));
                }
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }

    private void sendButtonClicked() {
        try {
            String text = clientText.getText();
            clientText.setText("");
            outputStream.writeUTF(text);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
