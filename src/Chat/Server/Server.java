package Chat.Server;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;

public class Server {

    public static void main(String[] args) {
        while (true){
            try {
                ServerSocket serverSocket = new ServerSocket(8000);
                System.out.println("waiting for people...");

                Socket socket1 = serverSocket.accept();
                System.out.println("Person1 Joined!");

                Socket socket2 = serverSocket.accept();
                System.out.println("Person2 Joined!");

                new Thread(new Chat(socket1, socket2)).start();
                new Thread(new Chat(socket2, socket1)).start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static class Chat implements Runnable {

        Socket socket1;
        Socket socket2;

        public Chat(Socket socket1, Socket socket2) {
            this.socket1 = socket1;
            this.socket2 = socket2;

        }

        @Override
        public synchronized void run() {
            try {
                DataInputStream from1 = new DataInputStream(socket1.getInputStream());
                DataOutputStream to1 = new DataOutputStream(socket1.getOutputStream());
                DataOutputStream to2 = new DataOutputStream(socket2.getOutputStream());
                while (true){
                    String massage = from1.readUTF();
                    System.out.println(massage+" received.");
                    to1.writeUTF("I said: "+massage);
                    to2.writeUTF("Another Client said: "+massage);
                    System.out.println(massage +" sent.");
            }
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
