package Client;

import Client.Account.Account;
import Client.Collection.AnchorPanes;
import Client.Collection.MediaViews;
import Client.Games.BattleShip.BattleShipGame_1;
import Client.Games.TicTacToe_1.Board;
import Client.Games.TicTacToe_1.Cell;
import Client.Games.TicTacToe_1.Tic_Tac_Toe_Game_1;
import Client.Games.TicTacToe_2.Tic_Tac_Toe_Game_2;
import animatefx.animation.*;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.ImageCursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

public class Controller extends Application {

    public static Scene MainScene;
    private Stage stage = new Stage();
    private ProgressBar progressBar;
    private double progressBarValue;
    private FadeTransition fadeinMenu;
    private Account account;
    public static int TicTacToe_1_situation = 0;
    public static int TicTacToe_2_situation = 0;
    public static int BattleShip_1_situation = 0;
    private String USERNAME;
    private String PASSWORD;

    //collections
    private AnchorPanes anchorPanes = new AnchorPanes();
    private MediaViews mediaViews = new MediaViews();

    //FILES PATH
    public static String media =System.getProperty("user.dir")+"\\src\\Client\\Media\\";
    public static String photo = "file:" + System.getProperty("user.dir")+ "\\src\\Client\\Photos\\";

    public Image cursor_game = new Image(Controller.photo+"GAMECURSOR.gif");
    public Image cursor_normal = new Image(Controller.photo+"MENUCURSOR.gif");


    //MEDIA PLAYERS _ MUSIC
    private double volume;

    public static Media menuMusic = new Media(new File(media +"menuMusic.mp3").toURI().toString());
    public static MediaPlayer menuMediaPlayer = new MediaPlayer(menuMusic);

    public static Media gameMusic = new Media(new File(media +"gameMusic.m4a").toURI().toString());
    public static MediaPlayer gameMediaPlayer = new MediaPlayer(gameMusic);

    public static AudioClip buttonEffect = new AudioClip("file:src/Client/media/click.wav");
    public static boolean SFXWasSetMute = false;

    //FXML COMPONENTS

    //Sign in
    @FXML private TextField nameFieldIn;
    @FXML private TextField passFieldIn;
    @FXML private Label reportIn;
    @FXML private Button enterBtn;

    //Sign up
    @FXML private TextField nameFieldUp;
    @FXML private TextField passFieldUp;
    @FXML private TextField emailFieldUp;
    @FXML private ChoiceBox chooseQUp;
    @FXML private TextField answerUp;
    @FXML private Label reportUp;
    @FXML private Button signUpBtn;
    @FXML private Button backUp;

    //RetrievalPass
    @FXML private TextField nameFieldRecover;
    @FXML private ChoiceBox chooseQRecover;
    @FXML private TextField answerFieldRecover;
    @FXML private Label reportRecover;
    @FXML private Button findPass;
    @FXML private Button backRecover;

    //Options
    @FXML private Slider volumeSlider;
    @FXML private MediaView optionMovie;
    @FXML private MediaView optionFront;

    //Game Options
    @FXML private Button tempExit;
    @FXML private Button tempBackMenu;
    @FXML private Button tempMute;


    //Choose Mod
    @FXML private MediaView chooseModMovie;
    @FXML private Button backMod;

    //Tic-tac-toeGameDisplay_1
    @FXML private MediaView tic_tac_toe_Display_1Movie;

    //Tic-tac-toeGameDisplay_2
    @FXML private TextArea typeArea;

    @FXML private MediaView Display_WIN;
    @FXML private MediaView Display_LOSE;
    @FXML private MediaView Display_DRAW;



    @Override
    //*************************************************************LOADING & MENU PART***********************************************************************//
    public void start(Stage primaryStage){

        AnchorPane loadingPane = new AnchorPane();
        loadingPane.getStylesheets().add(Controller.class.getResource("Styles/LoadingStyle.css").toExternalForm());
        Font.loadFont("file:src/Client/Font/UECHIGOT.TTF", 20);
        Font.loadFont("file:src/Client/Font/Gabriola.ttf", 20);


        MediaView LoadingMovie = new MediaView(mediaViews.getLoadingPlayer());
        mediaViews.getLoadingPlayer().setCycleCount(5);

        anchorPanes.setMenuPane(setMenuPane());
        anchorPanes.getMenuPane().setCursor(new ImageCursor(cursor_normal));

        //Play Main Music
        menuMediaPlayer.play();
        menuMediaPlayer.setVolume(0.5);
        menuMediaPlayer.setCycleCount(1000);

        progressBar = new ProgressBar();
        progressBar.getStyleClass().add("progressBar");
        progressBar.setPrefWidth(1650);
        progressBar.setLayoutX(140);
        progressBar.setLayoutY(1000);
        progressBarValue = 0.01;

        loadingPane.getChildren().addAll(LoadingMovie, progressBar);
        mediaViews.getLoadingPlayer().play();

        //Spend Time For Progress
        Timeline progressTimeline = new Timeline(new KeyFrame(Duration.millis(20), ae -> {
            progressBarValue *= 1.02;
            progressBar.setProgress(progressBarValue);
        }));

        //After Finish  The Progress Go To Main Page
        progressTimeline.setOnFinished(event -> {
            MainScene.setRoot(anchorPanes.getMenuPane());
            anchorPanes.getMenuPane().setCursor(new ImageCursor(cursor_normal));

            mediaViews.getMenuPlayer().play();
            mediaViews.getMenuPlayer().setCycleCount(1000);
            fadeinMenu.play();
            Parent signIn = null;
            try {
                signIn = FXMLLoader.load(getClass().getResource("FXMLfiles/SignInFXML.fxml"));
            }catch (Exception e){
                e.printStackTrace();
            }
            anchorPanes.getMenuPane().setOpacity(0.5);
            anchorPanes.getSignInPane().getChildren().addAll(anchorPanes.getMenuPane(), signIn);
            anchorPanes.getSignInPane().setOpacity(0);
            MainScene.setRoot(anchorPanes.getSignInPane());
            anchorPanes.getSignInPane().setCursor(new ImageCursor(cursor_normal));

            Timeline fadeInSignIn = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getSignInPane().setOpacity(anchorPanes.getSignInPane().getOpacity()+0.05);
            }));

            fadeInSignIn.setCycleCount(20);
            fadeInSignIn.play();
        });

        MainScene = new Scene(loadingPane, 1900, 1000);
        stage.setScene(MainScene);
        openStage();

        //Start Progress
        progressTimeline.setCycleCount(233);
        progressTimeline.play();

    }

    //Open Menu Page
    public void backToMenu_Buttons(){
        gameMediaPlayer.stop();
        menuMediaPlayer.play();
        clickSound();
        anchorPanes.setMenuPane(setMenuPane());
        MainScene.setRoot(anchorPanes.getMenuPane());
        anchorPanes.getMenuPane().setCursor(new ImageCursor(cursor_normal));
        fadeinMenu.setFromValue(0);
        fadeinMenu.setToValue(1);
        fadeinMenu.play();
        mediaViews.getMenuPlayer().play();
        mediaViews.getMenuPlayer().setCycleCount(1000);
    }
    public void backToMenu(){
        gameMediaPlayer.stop();
        menuMediaPlayer.play();
        anchorPanes.setMenuPane(setMenuPane());
        MainScene.setRoot(anchorPanes.getMenuPane());
        anchorPanes.getMenuPane().setCursor(new ImageCursor(cursor_normal));
        fadeinMenu.setFromValue(0);
        fadeinMenu.setToValue(1);
        fadeinMenu.play();
        mediaViews.getMenuPlayer().play();
        mediaViews.getMenuPlayer().setCycleCount(1000);
    }
    public AnchorPane setMenuPane(){

        MediaView MenuMovie = new MediaView( mediaViews.getMenuPlayer());
        mediaViews.getMenuPlayer().setCycleCount(1000);
        fadeinMenu = new FadeTransition(Duration.millis(2000), MenuMovie);
        fadeinMenu.setFromValue(0);
        fadeinMenu.setToValue(1);
        AnchorPane anchorPane = new AnchorPane(MenuMovie);
        anchorPane.getStylesheets().add(Controller.class.getResource("Styles/MenuStyle.css").toExternalForm());
        fadeinMenu.setOnFinished(event1 -> {

            Label GAME = new Label("Game");                                                        //GAME LABEL -> GO TO CHOOSE MOD
            setMenuButtonLabel(GAME, 1248, 310);
            GAME.setOnMouseClicked(event -> openChooseGame());

            Label CHAT = new Label("Chat");                                                        //CHAT LABEL -> GO TO CHAT PART
            setMenuButtonLabel(CHAT, 1270, 450);
            CHAT.setOnMouseClicked(event -> backToMenu_Buttons());

            Label OPTIONS = new Label("Option");                                                   //OPTIONS LABEL -> GO SETTING
            setMenuButtonLabel(OPTIONS, 1236, 593);
            OPTIONS.setOnMouseClicked(event -> openSetting());

            Label EXIT = new Label("Exit");                                                        //EXIT LABEL -> QUIT PROGRAM
            EXIT.setOnMouseClicked(event -> {
                clickSound();
                System.exit(0);
            });
            setMenuButtonLabel(EXIT, 1270, 735);

            Label Erienne = new Label("Erienne");
            setMenuButtonLabel(Erienne, 1120, 80);
            Erienne.setId("ErienneLabel");

            anchorPane.getChildren().addAll(GAME, CHAT, OPTIONS, EXIT,Erienne);

        });
        return anchorPane;
    }


    //**********************************************************************OPTION PART***********************************************************************//
    //Open Option Page
    public void openSetting() {
        clickSound();
        try {
            //open fxml file
            Parent optionRoot = FXMLLoader.load(getClass().getResource("FXMLfiles/OptionsFXML.fxml"));

            //play playback movie
            optionMovie = new MediaView(mediaViews.getOptionPlayer());
            mediaViews.getOptionPlayer().setCycleCount(1000);
            mediaViews.getOptionPlayer().play();

            //set fade in animation for pane
            optionFront = new MediaView(mediaViews.getOptionPlayerFront());
            mediaViews.getOptionPlayerFront().setCycleCount(1000);
            mediaViews.getOptionPlayerFront().play();

            anchorPanes.getOptionPane().getChildren().addAll(optionMovie,optionFront, optionRoot);

            optionFront.setOpacity(0.3);
            optionFront.setLayoutX(0);
            optionFront.setLayoutY(0);
            optionFront.setFitHeight(1200);
            optionFront.setFitWidth(2100);

            anchorPanes.getOptionPane().setOpacity(0);
            MainScene.setRoot(anchorPanes.getOptionPane());
            anchorPanes.getOptionPane().setCursor(new ImageCursor(cursor_normal));
            Timeline fadeInSignIn = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getOptionPane().setOpacity(anchorPanes.getOptionPane().getOpacity()+0.05);
            }));

            fadeInSignIn.setCycleCount(20);
            fadeInSignIn.play();

        } catch (IOException e) {
            backToMenu_Buttons();
        }
    }

    //Option Setting
    public void setMusicCheckBoxAction(){           //mute game musics
        clickSound();
        if (menuMediaPlayer.isMute()){
            menuMediaPlayer.setMute(false);
            gameMediaPlayer.setMute(false);
        }
        else {
            menuMediaPlayer.setMute(true);
            gameMediaPlayer.setMute(true);
        }
    }
    public void setSFXCheckBoxAction(){             //mute game effects
        clickSound();
        if (!SFXWasSetMute){
            buttonEffect.setVolume(0);
            SFXWasSetMute = true;
        }
        else {
            buttonEffect.setVolume(volume);
            SFXWasSetMute = false;
        }

    }
    public void setVolumeSliderAction() {           //set volume
        volume = volumeSlider.getValue()/100;
        menuMediaPlayer.setVolume(volume);
        if (!SFXWasSetMute)
            buttonEffect.setVolume(volume);
    }


    //***************************************************************SIGN PU & SIGN IN PART*******************************************************************//

    //Open Sign Pn Page
    public void goSignIn(){
        clickSound();
        Parent signIn = null;
        try {
            signIn = FXMLLoader.load(getClass().getResource("FXMLfiles/SignInFXML.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        AnchorPane anchorPane = new AnchorPane(setMenuPane());
        mediaViews.getMenuPlayer().play();
        anchorPane.setOpacity(0.5);
        fadeinMenu.play();

        anchorPanes.getSignInPane().getChildren().addAll(anchorPane, signIn);

        anchorPanes.getSignInPane().setOpacity(0);
        Controller.MainScene.setRoot(anchorPanes.getSignInPane());
        anchorPanes.getSignInPane().setCursor(new ImageCursor(cursor_normal));
        Timeline fadeInSignIn = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
            anchorPanes.getSignInPane().setOpacity(anchorPanes.getSignInPane().getOpacity()+0.05);
        }));

        fadeInSignIn.setCycleCount(20);
        fadeInSignIn.play();
    }

    //Open Sign Up Page
    public void goSignUp(){
        clickSound();
        Parent signUp = null;
        try {
            signUp = FXMLLoader.load(getClass().getResource("FXMLfiles/SignUpFXML.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        AnchorPane anchorPane = new AnchorPane(setMenuPane());
        mediaViews.getMenuPlayer().play();
        anchorPane.setOpacity(0.5);
        fadeinMenu.play();

        anchorPanes.getSignUpPane().getChildren().addAll(anchorPane, signUp);

        anchorPanes.getSignUpPane().setOpacity(0);
        Controller.MainScene.setRoot(anchorPanes.getSignUpPane());
        anchorPanes.getSignUpPane().setCursor(new ImageCursor(cursor_normal));
        Timeline fadeInSignUp = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
            anchorPanes.getSignUpPane().setOpacity(anchorPanes.getSignUpPane().getOpacity()+0.05);
        }));

        fadeInSignUp.setCycleCount(20);
        fadeInSignUp.play();
    }

    //Open Retrieval Password Page
    public void goRetrievalPass(){
        clickSound();
        Parent passRecover = null;
        try {
            passRecover = FXMLLoader.load(getClass().getResource("FXMLfiles/RetrievalPassFXML.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        AnchorPane anchorPane = new AnchorPane(setMenuPane());
        mediaViews.getMenuPlayer().play();
        anchorPane.setOpacity(0.5);
        fadeinMenu.play();

        anchorPanes.getRetrievalPassPane().getChildren().addAll(anchorPane, passRecover);

        anchorPanes.getRetrievalPassPane().setOpacity(0);
        Controller.MainScene.setRoot(anchorPanes.getRetrievalPassPane());
        anchorPanes.getRetrievalPassPane().setCursor(new ImageCursor(cursor_normal));
        Timeline fadeInretrievalPass = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
            anchorPanes.getRetrievalPassPane().setOpacity(anchorPanes.getRetrievalPassPane().getOpacity()+0.05);
        }));

        fadeInretrievalPass.setCycleCount(20);
        fadeInretrievalPass.play();
    }

    //Sign Up and Go Menu After Sign Up
    //send user information to server and read result:
    //-1 -> if username exist
    // 1 -> if user saved successfully
    public void exitSignUp() {
        clickSound();
        try {
            //connect to server to check user information and sign up
            Socket socket = new Socket("localhost", 8000);
            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            String username = nameFieldUp.getText();
            String pass = passFieldUp.getText();
            String email = emailFieldUp.getText();
            String question = (String) chooseQUp.getValue();
            String answer = answerUp.getText();
            System.out.println("New User: "+username + "   " + pass + "   " + email + "   " + question + "   " + answer);

            toServer.writeUTF(username);
            toServer.writeUTF(pass);
            toServer.writeUTF(email);
            toServer.writeUTF(question);
            toServer.writeUTF(answer);
            String result = fromServer.readUTF();

            if (result.equals("-1")) reportUp.setText("  username exist.");
            else if (result.equals("1")) {
                this.account = new Account(username, pass, email, question, answer);
                USERNAME =username;
                PASSWORD = pass;
                backToMenu();
            }

        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    //Go Menu After Sign In
    //send username and password to server and read result:
    //-1 -> if username doesn't exist
    // 0 -> if username and password are not match
    // 1 -> if username and password match
    public void exitSignIn(){
        clickSound();
        try {
            //connect to server to check user information and sign in
            Socket socket = new Socket("localhost", 9000);
            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            String username = nameFieldIn.getText();
            String pass = passFieldIn.getText();

            System.out.println("Oline User: "+username +"   "+pass);

            toServer.writeUTF(username);
            toServer.writeUTF(pass);

            String result = fromServer.readUTF();
            if (result.equals("-1"))reportIn.setText("         username doesn't exist.");
            else if (result.equals("0"))reportIn.setText("username and password are not match.");
            else if (result.equals("1")){
                USERNAME =username;
                PASSWORD = pass;
                backToMenu();
            }

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //Show the pass
    //send username, question and answer to server and read result:
    //-1 -> if username doesn't exist
    // 0 -> if question and answer are not match
    // 1 -> if question and answer match
    public void showPass(){
        clickSound();
        try {
            //connect to server to check user information and show the pass
            Socket socket = new Socket("localhost", 7000);
            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            String username = nameFieldRecover.getText();
            String question = (String) chooseQRecover.getValue();
            String answer = answerFieldRecover.getText();

            System.out.println(username +"   "+question+"   "+answer);

            toServer.writeUTF(username);
            toServer.writeUTF(question);
            toServer.writeUTF(answer);

            String result = fromServer.readUTF();
            if (result.equals("-1"))reportRecover.setText("         username doesn't exist.");
            else if (result.equals("0"))reportRecover.setText("question and answer are not match.");
            else if (result.equals("1")){
                String pass = fromServer.readUTF();
                reportRecover.setText("password is:  "+pass);
            }

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    //animations
    public void enterBtnAnimation(){
        new Tada(enterBtn).play();
    }
    public void findPassAnimation(){
        new Tada(findPass).play();
    }
    public void signUpBtnAnimation(){
        new Tada(signUpBtn).play();
    }
    public void backUpAnimation(){
        new Tada(backUp).play();
    }
    public void backRecoverAnimation(){
        new Tada(backRecover).play();
    }
    public void backModAnimation(){
        new Tada(backMod).play();
    }


    //*********************************************************************GAMES PART*************************************************************************//
    //Open Games List
    public void openChooseGame(){
        clickSound();
        try {
            Parent gameList = FXMLLoader.load(getClass().getResource("FXMLfiles/GamesListFXML.fxml"));

            anchorPanes.getGameLists().getChildren().addAll(gameList);
            anchorPanes.getGameLists().setOpacity(0);
            MainScene.setRoot( anchorPanes.getGameLists());
            anchorPanes.getGameLists().setCursor(new ImageCursor(cursor_normal));
            Timeline fadeInGameList = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getGameLists().setOpacity(anchorPanes.getGameLists().getOpacity()+0.05);
            }));

            fadeInGameList.setCycleCount(20);
            fadeInGameList.play();

        } catch (IOException e) {
            backToMenu_Buttons();
        }
    }

    //open win Page
    public void win(){
        clickSound();
        gameMediaPlayer.stop();
        menuMediaPlayer.play();

        anchorPanes.setWinPane(new AnchorPane());
        Parent win = null;
        try {
            win = FXMLLoader.load(getClass().getResource("FXMLfiles/WinTicTacToePaneFXML.fxml"));
            Display_WIN = new MediaView(mediaViews.getWINPlayer());

            Display_WIN.setFitHeight(810);
            Display_WIN.setFitHeight(655);
            Display_WIN.setLayoutX(625);
            Display_WIN.setLayoutY(200);

            mediaViews.getWINPlayer().setCycleCount(1000);
            mediaViews.getWINPlayer().play();
        }catch (Exception e){
            e.printStackTrace();
        }
        AnchorPane anchorPane = new AnchorPane(setMenuPane());
        mediaViews.getMenuPlayer().play();
        anchorPane.setOpacity(0.5);
        fadeinMenu.play();

        anchorPanes.getWinPane().getChildren().addAll(anchorPane, Display_WIN, win);

        anchorPanes.getWinPane().setOpacity(0);
        Controller.MainScene.setRoot(anchorPanes.getWinPane());
        anchorPanes.getWinPane().setCursor(new ImageCursor(cursor_normal));

        Timeline fadeInWin = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
            anchorPanes.getWinPane().setOpacity(anchorPanes.getWinPane().getOpacity()+0.05);
        }));

        fadeInWin.setCycleCount(20);
        fadeInWin.play();
        this.account = new Account(USERNAME, PASSWORD);
        account.addResult("tictactoe", 1);
    }

    //Open Lose Page
    public void lose(){
        clickSound();
        gameMediaPlayer.stop();
        menuMediaPlayer.play();

        anchorPanes.setLosePane(new AnchorPane());
        Parent lose = null;
        try {
            lose = FXMLLoader.load(getClass().getResource("FXMLfiles/LoseTicTacToePaneFXML.fxml"));
            Display_LOSE = new MediaView(mediaViews.getLOSEPlayer());

            Display_LOSE.setFitHeight(810);
            Display_LOSE.setFitHeight(655);
            Display_LOSE.setLayoutX(625);
            Display_LOSE.setLayoutY(200);

            mediaViews.getLOSEPlayer().setCycleCount(1000);
            mediaViews.getLOSEPlayer().play();
        }catch (Exception e){
            e.printStackTrace();
        }
        AnchorPane anchorPane = new AnchorPane(setMenuPane());
        mediaViews.getMenuPlayer().play();
        anchorPane.setOpacity(0.5);
        fadeinMenu.play();

        anchorPanes.getLosePane().getChildren().addAll(anchorPane, Display_LOSE, lose);

        anchorPanes.getLosePane().setOpacity(0);
        Controller.MainScene.setRoot(anchorPanes.getLosePane());
        anchorPanes.getLosePane().setCursor(new ImageCursor(cursor_normal));

        Timeline fadeInLose = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
            anchorPanes.getLosePane().setOpacity(anchorPanes.getLosePane().getOpacity()+0.05);
        }));

        fadeInLose.setCycleCount(20);
        fadeInLose.play();
    }

    //Open Draw Page
    public void draw(){
        clickSound();
        gameMediaPlayer.stop();
        menuMediaPlayer.play();

        anchorPanes.setDrawPane(new AnchorPane());
        Parent draw = null;
        try {
            draw = FXMLLoader.load(getClass().getResource("FXMLfiles/DrawTicTacToePaneFXML.fxml"));
            Display_DRAW = new MediaView(mediaViews.getDRAWPlayer());

            Display_DRAW.setFitHeight(810);
            Display_DRAW.setFitHeight(655);
            Display_DRAW.setLayoutX(625);
            Display_DRAW.setLayoutY(200);

            mediaViews.getDRAWPlayer().setCycleCount(1000);
            mediaViews.getDRAWPlayer().play();

        }catch (Exception e){
            e.printStackTrace();
        }
        AnchorPane anchorPane = new AnchorPane(setMenuPane());
        mediaViews.getMenuPlayer().play();
        anchorPane.setOpacity(0.5);
        fadeinMenu.play();

        anchorPanes.getDrawPane().getChildren().addAll(anchorPane, Display_DRAW, draw);

        anchorPanes.getDrawPane().setOpacity(0);
        Controller.MainScene.setRoot(anchorPanes.getDrawPane());
        anchorPanes.getDrawPane().setCursor(new ImageCursor(cursor_normal));

        Timeline fadeInDraw = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
            anchorPanes.getDrawPane().setOpacity(anchorPanes.getDrawPane().getOpacity()+0.05);
        }));

        fadeInDraw.setCycleCount(20);
        fadeInDraw.play();
    }



    //****************************************************************TIC_TAC_TOE GAME PART*******************************************************************//
    //Open Choose Mod Page
    public void openChooseModTic(){
        clickSound();
        try {
            Parent chooseModRoot = FXMLLoader.load(getClass().getResource("FXMLfiles/ChooseModTicFXML.fxml"));
            chooseModMovie = new MediaView(mediaViews.getChooseModPlayer());
            mediaViews.getChooseModPlayer().setCycleCount(1000);
            mediaViews.getChooseModPlayer().play();

            anchorPanes.getChooseModPane().getChildren().addAll(chooseModMovie, chooseModRoot);
            anchorPanes.getChooseModPane().setOpacity(0);
            MainScene.setRoot( anchorPanes.getChooseModPane());
            anchorPanes.getChooseModPane().setCursor(new ImageCursor(cursor_normal));
            Timeline fadeInChooseMod = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getChooseModPane().setOpacity( anchorPanes.getChooseModPane().getOpacity()+0.05);
            }));

            fadeInChooseMod.setCycleCount(20);
            fadeInChooseMod.play();

        } catch (IOException e) {
            backToMenu_Buttons();
        }
    }

    //Open 1Player tic-tac-toe game
    public void goToGameTic_Tac_Toe_1(){
        clickSound();
        gameMediaPlayer.play();
        menuMediaPlayer.stop();
        anchorPanes.setTic_tac_toePane_1(new AnchorPane());
        TicTacToe_1_situation = 0;
        try {
            Parent tic_tac_toeRoot = FXMLLoader.load(getClass().getResource("FXMLfiles/TictactoeGameDisplay_1.fxml"));
            tic_tac_toe_Display_1Movie = new MediaView(mediaViews.getTic_tac_toePlayer());
            mediaViews.getTic_tac_toePlayer().setCycleCount(1000);
            mediaViews.getTic_tac_toePlayer().play();

            anchorPanes.getTic_tac_toePane_1().getChildren().addAll(tic_tac_toe_Display_1Movie, tic_tac_toeRoot);
            anchorPanes.getTic_tac_toePane_1().setOpacity(0);
            MainScene.setRoot(anchorPanes.getTic_tac_toePane_1());
            anchorPanes.getTic_tac_toePane_1().setCursor(new ImageCursor(cursor_game));
            Timeline fadeInTic_tac_toeDisplay_1 = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getTic_tac_toePane_1().setOpacity(anchorPanes.getTic_tac_toePane_1().getOpacity()+0.05);
            }));

            fadeInTic_tac_toeDisplay_1.setCycleCount(20);
            fadeInTic_tac_toeDisplay_1.play();

            anchorPanes.getTic_tac_toePane_1().getChildren().add(new Board(980, 150, event -> {
                Cell cell = (Cell) event.getSource();
                new Tic_Tac_Toe_Game_1(cell.getRow(), cell.getColumn()).run();
                if (TicTacToe_1_situation != 0){
                    if (TicTacToe_1_situation == 1)win();
                    else if (TicTacToe_1_situation == 2)lose();
                    else if (TicTacToe_1_situation == 3)draw();
                }
            }));

        } catch (IOException e) {
            backToMenu_Buttons();
        }
    }

    public static DataInputStream chatFrom;
    public static DataOutputStream chatTo;

    public void goToGameTic_Tac_Toe_2(){
        clickSound();
        gameMediaPlayer.play();
        menuMediaPlayer.stop();
        anchorPanes.setTic_tac_toePane_2(new AnchorPane());
        TicTacToe_2_situation = 0;
        try {
            Parent tic_tac_toeRoot = FXMLLoader.load(getClass().getResource("FXMLfiles/TictactoeGameDisplay_2.fxml"));
            tic_tac_toe_Display_1Movie = new MediaView(mediaViews.getTic_tac_toePlayer());
            mediaViews.getTic_tac_toePlayer().setCycleCount(1000);
            mediaViews.getTic_tac_toePlayer().play();

            anchorPanes.getTic_tac_toePane_2().getChildren().addAll(tic_tac_toe_Display_1Movie, tic_tac_toeRoot);
            anchorPanes.getTic_tac_toePane_2().setOpacity(0);
            MainScene.setRoot(anchorPanes.getTic_tac_toePane_2());
            anchorPanes.getTic_tac_toePane_2().setCursor(new ImageCursor(cursor_game));
            Timeline fadeInTic_tac_toeDisplay_2 = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getTic_tac_toePane_2().setOpacity(anchorPanes.getTic_tac_toePane_2().getOpacity()+0.05);
            }));

            fadeInTic_tac_toeDisplay_2.setCycleCount(20);
            fadeInTic_tac_toeDisplay_2.play();

        } catch (IOException e) {
            backToMenu_Buttons();
        }
        try {

            //connect to server to play tic tac toe online
            Socket gameSocket = new Socket("localhost", 10000);
            Label label = new Label();

            Tic_Tac_Toe_Game_2 gameDisplay = new Tic_Tac_Toe_Game_2(gameSocket, label);

            TextArea showArea = new TextArea();
            label.getStylesheets().add(Controller.class.getResource("Styles/ChatTicStyle.css").toExternalForm());
            showArea.setEditable(false);
            showArea.setWrapText(true);
            showArea.setLayoutX(1365);
            showArea.setLayoutY(30);
            showArea.setPrefWidth(500);
            showArea.setPrefHeight(690);
            showArea.setOpacity(0.6);

            gameDisplay.setLayoutX(355);
            gameDisplay.setLayoutY(100);
            anchorPanes.getTic_tac_toePane_2().getChildren().addAll(gameDisplay, label, showArea);
            label.setFont(Font.font("Uechi-Gothic", 60));

            label.getStylesheets().add(Controller.class.getResource("Styles/ChatTicStyle.css").toExternalForm());
            label.setPrefWidth(1296);
            label.setPrefHeight(118);
            label.setLayoutX(327);
            label.setLayoutY(725);
            label.setTextAlignment(TextAlignment.CENTER);

            MainScene.setRoot(anchorPanes.getTic_tac_toePane_2());

            //connect to server in another port to chat with his/her friend
            Socket chatSocket = new Socket("localhost", 10500);
            chatFrom = new DataInputStream(chatSocket.getInputStream());
            chatTo = new DataOutputStream(chatSocket.getOutputStream());

            //to read another chat messages
            new Thread(() -> {
                while (true){
                    String text = null;
                    try {
                        text = chatFrom.readUTF();
                        System.out.println(text);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String finalText = text;
                    Platform.runLater(() -> showArea.appendText( finalText + "\n"));
                }
            }).start();


        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //after click button, message send to server to send to his/her friend
    public void sendChatAction(){
        try {
            String text = typeArea.getText();
            typeArea.setText("");
            chatTo.writeUTF(text);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

//    public void endTicTacToeGame(int result){
//        if (result == 1)win();
//        else if (result == 3)draw();
//        else if (result == 2)lose();
//    }


    //****************************************************************BATTLE SHIP GAME PART*******************************************************************//
    //Open Choose Mod Page
    public void openChooseModBattle(){
        clickSound();
        try {
            Parent chooseModRoot = FXMLLoader.load(getClass().getResource("FXMLfiles/ChooseModBattleFXML.fxml"));
            chooseModMovie = new MediaView(mediaViews.getChooseModPlayer());
            mediaViews.getChooseModPlayer().setCycleCount(1000);
            mediaViews.getChooseModPlayer().play();

            anchorPanes.getChooseModPane().getChildren().addAll(chooseModMovie, chooseModRoot);
            anchorPanes.getChooseModPane().setOpacity(0);
            MainScene.setRoot( anchorPanes.getChooseModPane());
            anchorPanes.getChooseModPane().setCursor(new ImageCursor(cursor_normal));
            Timeline fadeInChooseMod = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getChooseModPane().setOpacity( anchorPanes.getChooseModPane().getOpacity()+0.05);
            }));

            fadeInChooseMod.setCycleCount(20);
            fadeInChooseMod.play();

        } catch (IOException e) {
            backToMenu_Buttons();
        }
    }

    //Open 1Player tic-tac-toe game
    public void goToGameBattleShip(){
        clickSound();
        gameMediaPlayer.play();
        menuMediaPlayer.stop();
        anchorPanes.setBattleShipPane(new AnchorPane());
        BattleShip_1_situation = 0;
        try {
            Parent battleRoot = FXMLLoader.load(getClass().getResource("FXMLfiles/BattleShipDisplayGameFXML.fxml"));

            BattleShipGame_1 battleShipGame = new BattleShipGame_1();
            battleShipGame.setOnMouseClicked(event -> endBattleShipGame());
            battleShipGame.setLayoutX(500);
            battleShipGame.setLayoutY(300);

            anchorPanes.getBattleShipPane().getChildren().addAll(battleRoot, battleShipGame);
            anchorPanes.getBattleShipPane().setOpacity(0);
            MainScene.setRoot( anchorPanes.getBattleShipPane());
            anchorPanes.getBattleShipPane().setCursor(new ImageCursor(cursor_game));
            Timeline fadeInChooseMod = new Timeline(new KeyFrame(Duration.millis(100), ae -> {
                anchorPanes.getBattleShipPane().setOpacity( anchorPanes.getBattleShipPane().getOpacity()+0.05);
            }));

            fadeInChooseMod.setCycleCount(20);
            fadeInChooseMod.play();

        } catch (IOException e) {
            backToMenu_Buttons();
        }
    }

    public void endBattleShipGame(){
        if (BattleShip_1_situation == -1)lose();
        else if (BattleShip_1_situation == 1)win();
    }


    //****************************************************************GAMES OPTIONS PART************************************************************************//
    public static void clickSound(){
        buttonEffect.play();
    }

    //to get sure that none of nameFieldUp and passFieldUp is empty
    @FXML public void handleEnter(){
        try {
            boolean isEmpty = nameFieldIn.getText().isEmpty() || nameFieldIn.getText().trim().isEmpty() ||
                    passFieldIn.getText().isEmpty() || passFieldIn.getText().trim().isEmpty();
            enterBtn.setDisable(isEmpty);
        }catch (Exception e){
            return;
        }
    }

    //to get sure that none of sign up page fields is empty
    @FXML public void handleSignUp(){
        try {
            boolean isEmpty = nameFieldUp.getText().isEmpty() || nameFieldUp.getText().trim().isEmpty() ||
                    passFieldUp.getText().isEmpty() || passFieldUp.getText().trim().isEmpty() ||
                    emailFieldUp.getText().isEmpty() || emailFieldUp.getText().trim().isEmpty() ||
                    answerUp.getText().isEmpty() || answerUp.getText().trim().isEmpty();
            signUpBtn.setDisable(isEmpty);
        }catch (Exception e){
            return;
        }
    }

    //to get sure that none of RetrievalPass page fields is empty
    @FXML public void handleRetrievalPass(){
        try {
            boolean isEmpty = nameFieldRecover.getText().isEmpty() || nameFieldRecover.getText().trim().isEmpty() ||
                    answerFieldRecover.getText().isEmpty() || answerFieldRecover.getText().trim().isEmpty();
            findPass.setDisable(isEmpty);
        }catch (Exception e){
            return;
        }
    }

    private void setMenuButtonLabel(Label label, int x, int y){
        label.setTextAlignment(TextAlignment.CENTER);
        label.setLayoutX(x);
        label.setLayoutY(y);
    }

    //options in game display
    private void openStage(){
        stage.getIcons().add(new Image(photo+"ICON2.png"));
        stage.setTitle("Erienne");
        stage.setFullScreen(true);
        stage.setResizable(true);
        stage.show();
    }

    private boolean optionIsComposed = true;

    public void composed(){
        clickSound();
        if (!optionIsComposed) {
            TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(1), tempMute);
            translateTransition1.setByX(+214);
            translateTransition1.setByY(-11);

            TranslateTransition translateTransition2 = new TranslateTransition(Duration.seconds(1), tempBackMenu);
            translateTransition2.setByX(+163);
            translateTransition2.setByY(-111);

            TranslateTransition translateTransition3 = new TranslateTransition(Duration.seconds(1), tempExit);
            translateTransition3.setByX(+14);
            translateTransition3.setByY(-170);

            translateTransition1.play();
            translateTransition2.play();
            translateTransition3.play();

            optionIsComposed = !optionIsComposed;
        } else {
            TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(1), tempMute);
            translateTransition1.setByX(-214);
            translateTransition1.setByY(+11);

            TranslateTransition translateTransition2 = new TranslateTransition(Duration.seconds(1), tempBackMenu);
            translateTransition2.setByX(-163);
            translateTransition2.setByY(+111);

            TranslateTransition translateTransition3 = new TranslateTransition(Duration.seconds(1), tempExit);
            translateTransition3.setByX(-14);
            translateTransition3.setByY(+170);

            translateTransition1.play();
            translateTransition2.play();
            translateTransition3.play();

            optionIsComposed = !optionIsComposed;
        }
    }

    public void TempMuteAction(){
        if (!gameMediaPlayer.isMute())gameMediaPlayer.setMute(true);
        else gameMediaPlayer.setMute(false);
    }

    public void TempExitAction(){
        System.exit(0);
    }

    public void TempGoMenuAction(){
        backToMenu_Buttons();
    }


    //emoji buttons
    public void addEmoji_1(){
        typeArea.appendText("\uD83D\uDE00");
    }

    public void addEmoji_2(){
        typeArea.appendText("\uD83D\uDE42");
    }

    public void addEmoji_3(){
        typeArea.appendText("\uD83D\uDE09");
    }

    public void addEmoji_4(){
        typeArea.appendText("\uD83D\uDE0C");
    }

    public void addEmoji_5(){
        typeArea.appendText("\uD83D\uDE06");
    }

    public void addEmoji_6(){
        typeArea.appendText("\uD83D\uDE04");
    }

    public void addEmoji_7(){
        typeArea.appendText("\uD83D\uDE44 ");
    }

    public void addEmoji_8(){
        typeArea.appendText("\uD83D\uDE12");
    }

    public void addEmoji_9(){
        typeArea.appendText("\uD83D\uDE05");
    }

    public void addEmoji_10(){
        typeArea.appendText("☺");
    }

    public void addEmoji_11(){
        typeArea.appendText("\uD83D\uDE28");
    }

    public void addEmoji_12(){
        typeArea.appendText("\uD83D\uDE10 ");
    }

    public void addEmoji_13(){
        typeArea.appendText("\uD83D\uDE0D");
    }

    public void addEmoji_14(){
        typeArea.appendText("\uD83D\uDE17");
    }

    public void addEmoji_15(){
        typeArea.appendText("\uD83D\uDE18");
    }

    public void addEmoji_16(){
        typeArea.appendText("\uD83E\uDD23");
    }

    public void addEmoji_17(){
        typeArea.appendText("\uD83E\uDD14");
    }

    public void addEmoji_18(){
        typeArea.appendText("\uD83D\uDE02");
    }
}
