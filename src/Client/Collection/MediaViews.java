package Client.Collection;

import Client.Controller;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class MediaViews {
    //LOADING
    private Media Loading = new Media(new File(Controller.media+"LOADING.mp4").toURI().toString());
    private MediaPlayer LoadingPlayer = new MediaPlayer(Loading);

    //MENU
    private Media Menu = new Media(new File(Controller.media+"MENU.mp4").toURI().toString());
    private MediaPlayer MenuPlayer = new MediaPlayer(Menu);

    //OPTION
    private Media Option = new Media(new File(Controller.media+"OPTION.mp4").toURI().toString());
    private MediaPlayer OptionPlayer = new MediaPlayer(Option);
    private Media OptionFront = new Media(new File(Controller.media+"OPTION_FRONT.mp4").toURI().toString());
    private MediaPlayer OptionPlayerFront = new MediaPlayer(OptionFront);

    //CHOOSE MOD
    private Media ChooseMod = new Media(new File(Controller.media+"CHOOSEMOD.mp4").toURI().toString());
    private MediaPlayer ChooseModPlayer = new MediaPlayer(ChooseMod);

    //TIC-TAC-TOE DISPLAY
    private Media Tic_tac_toe = new Media(new File(Controller.media+"TICTACTOEDISPLAY_1.mp4").toURI().toString());
    private MediaPlayer Tic_tac_toePlayer = new MediaPlayer(Tic_tac_toe);

    //TIC-TAC-TOE WIN
    private Media WIN = new Media(new File(Controller.media+"TICTACTOEWIN.mp4").toURI().toString());
    private MediaPlayer WINPlayer = new MediaPlayer(WIN);

    //TIC-TAC-TOE LOSE
    private Media LOSE = new Media(new File(Controller.media+"TICTACTOELOSE.mp4").toURI().toString());
    private MediaPlayer LOSEPlayer = new MediaPlayer(LOSE);

    //TIC-TAC-TOE DRAW
    private Media DRAW = new Media(new File(Controller.media+"TICTACTOEDRAW.mp4").toURI().toString());
    private MediaPlayer DRAWPlayer = new MediaPlayer(DRAW);

    //BATTLE SHIP DISPLAY
    private Media BattleShip = new Media(new File(Controller.media+"TICTACTOEDISPLAY_1.mp4").toURI().toString());
    private MediaPlayer BattleShipPlayer = new MediaPlayer(BattleShip);

    public Media getLoading() {
        return Loading;
    }

    public void setLoading(Media loading) {
        Loading = loading;
    }

    public MediaPlayer getLoadingPlayer() {
        return LoadingPlayer;
    }

    public void setLoadingPlayer(MediaPlayer loadingPlayer) {
        LoadingPlayer = loadingPlayer;
    }

    public Media getMenu() {
        return Menu;
    }

    public void setMenu(Media menu) {
        Menu = menu;
    }

    public MediaPlayer getMenuPlayer() {
        return MenuPlayer;
    }

    public void setMenuPlayer(MediaPlayer menuPlayer) {
        MenuPlayer = menuPlayer;
    }

    public Media getOption() {
        return Option;
    }

    public void setOption(Media option) {
        Option = option;
    }

    public MediaPlayer getOptionPlayer() {
        return OptionPlayer;
    }

    public void setOptionPlayer(MediaPlayer optionPlayer) {
        OptionPlayer = optionPlayer;
    }

    public Media getChooseMod() {
        return ChooseMod;
    }

    public void setChooseMod(Media chooseMod) {
        ChooseMod = chooseMod;
    }

    public MediaPlayer getChooseModPlayer() {
        return ChooseModPlayer;
    }

    public void setChooseModPlayer(MediaPlayer chooseModPlayer) {
        ChooseModPlayer = chooseModPlayer;
    }

    public Media getTic_tac_toe() {
        return Tic_tac_toe;
    }

    public void setTic_tac_toe(Media tic_tac_toe) {
        Tic_tac_toe = tic_tac_toe;
    }

    public MediaPlayer getTic_tac_toePlayer() {
        return Tic_tac_toePlayer;
    }

    public void setTic_tac_toePlayer(MediaPlayer tic_tac_toePlayer) {
        Tic_tac_toePlayer = tic_tac_toePlayer;
    }

    public Media getWIN() {
        return WIN;
    }

    public void setWIN(Media WIN) {
        this.WIN = WIN;
    }

    public MediaPlayer getWINPlayer() {
        return WINPlayer;
    }

    public void setWINPlayer(MediaPlayer WINPlayer) {
        this.WINPlayer = WINPlayer;
    }

    public Media getLOSE() {
        return LOSE;
    }

    public void setLOSE(Media LOSE) {
        this.LOSE = LOSE;
    }

    public MediaPlayer getLOSEPlayer() {
        return LOSEPlayer;
    }

    public void setLOSEPlayer(MediaPlayer LOSEPlayer) {
        this.LOSEPlayer = LOSEPlayer;
    }

    public Media getDRAW() {
        return DRAW;
    }

    public void setDRAW(Media DRAW) {
        this.DRAW = DRAW;
    }

    public MediaPlayer getDRAWPlayer() {
        return DRAWPlayer;
    }

    public void setDRAWPlayer(MediaPlayer DRAWPlayer) {
        this.DRAWPlayer = DRAWPlayer;
    }

    public Media getOptionFront() {
        return OptionFront;
    }

    public void setOptionFront(Media optionFront) {
        OptionFront = optionFront;
    }

    public MediaPlayer getOptionPlayerFront() {
        return OptionPlayerFront;
    }

    public void setOptionPlayerFront(MediaPlayer optionPlayerFront) {
        OptionPlayerFront = optionPlayerFront;
    }

    public Media getBattleShip() {
        return BattleShip;
    }

    public void setBattleShip(Media battleShip) {
        BattleShip = battleShip;
    }

    public MediaPlayer getBattleShipPlayer() {
        return BattleShipPlayer;
    }

    public void setBattleShipPlayer(MediaPlayer battleShipPlayer) {
        BattleShipPlayer = battleShipPlayer;
    }
}
