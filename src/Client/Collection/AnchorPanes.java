package Client.Collection;

import javafx.scene.layout.AnchorPane;

public class AnchorPanes {

    public AnchorPanes() {
    }

    private AnchorPane menuPane = new AnchorPane();
    private AnchorPane optionPane = new AnchorPane();
    private AnchorPane signInPane = new AnchorPane();
    private AnchorPane signUpPane = new AnchorPane();
    private AnchorPane retrievalPassPane = new AnchorPane();
    private AnchorPane gameLists = new AnchorPane();
    private AnchorPane chooseModPane = new AnchorPane();
    private AnchorPane tic_tac_toePane_1 = new AnchorPane();
    private AnchorPane winPane = new AnchorPane();
    private AnchorPane losePane = new AnchorPane();
    private AnchorPane drawPane = new AnchorPane();
    private AnchorPane battleShipPane = new AnchorPane();
    private AnchorPane tic_tac_toePane_2 = new AnchorPane();

    public AnchorPane getBattleShipPane() {
        return battleShipPane;
    }

    public void setBattleShipPane(AnchorPane battleShipPane) {
        this.battleShipPane = battleShipPane;
    }

    public AnchorPane getMenuPane() {
        return menuPane;
    }

    public void setMenuPane(AnchorPane menuPane) {
        this.menuPane = menuPane;
    }

    public AnchorPane getOptionPane() {
        return optionPane;
    }

    public void setOptionPane(AnchorPane optionPane) {
        this.optionPane = optionPane;
    }

    public AnchorPane getSignInPane() {
        return signInPane;
    }

    public void setSignInPane(AnchorPane signInPane) {
        this.signInPane = signInPane;
    }

    public AnchorPane getSignUpPane() {
        return signUpPane;
    }

    public void setSignUpPane(AnchorPane signUpPane) {
        this.signUpPane = signUpPane;
    }

    public AnchorPane getRetrievalPassPane() {
        return retrievalPassPane;
    }

    public void setRetrievalPassPane(AnchorPane retrievalPassPane) {
        this.retrievalPassPane = retrievalPassPane;
    }

    public AnchorPane getChooseModPane() {
        return chooseModPane;
    }

    public void setChooseModPane(AnchorPane chooseModPane) {
        this.chooseModPane = chooseModPane;
    }

    public AnchorPane getTic_tac_toePane_1() {
        return tic_tac_toePane_1;
    }

    public void setTic_tac_toePane_1(AnchorPane tic_tac_toePane_1) {
        this.tic_tac_toePane_1 = tic_tac_toePane_1;
    }

    public AnchorPane getWinPane() {
        return winPane;
    }

    public void setWinPane(AnchorPane winPane) {
        this.winPane = winPane;
    }

    public AnchorPane getLosePane() {
        return losePane;
    }

    public void setLosePane(AnchorPane losePane) {
        this.losePane = losePane;
    }

    public AnchorPane getDrawPane() {
        return drawPane;
    }

    public void setDrawPane(AnchorPane drawPane) {
        this.drawPane = drawPane;
    }

    public AnchorPane getGameLists() {
        return gameLists;
    }

    public void setGameLists(AnchorPane gameLists) {
        this.gameLists = gameLists;
    }

    public AnchorPane getTic_tac_toePane_2() {
        return tic_tac_toePane_2;
    }

    public void setTic_tac_toePane_2(AnchorPane tic_tac_toePane_2) {
        this.tic_tac_toePane_2 = tic_tac_toePane_2;
    }
}
