package Client.Account;

import java.io.IOException;
import java.sql.*;

public class Account {
    private String username;
    private String password;
    private String question;
    private String email;
    private String answer;
    private Connection gameData;
    private Statement statement;
    private String URL;

    public Account(String username, String password,String email, String question, String answer) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.question = question;
        this.answer = answer;

        Connection gameData = null;
        try {
            this.URL = "jdbc:sqlite:src/Client/Account/"+this.username +".db";
            gameData = DriverManager.getConnection(URL);
            Statement statement = gameData.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS oflineGames(gameName TEXT, win INTEGER, lose INTEGER, equal TEXT)");
            if (!isThere("tictoctoe"))
                statement.execute("INSERT INTO oflineGames VALUES ('tictoctoe', 0, 0, 0)");
            gameData.close();

        } catch (SQLException e) {
            System.out.println("Can't connect to save your data!\n Please try again later!");
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    public Account(String username, String password) {
        this.username = username;
        this.password = password;

        Connection gameData = null;
        try {
            this.URL = "jdbc:sqlite:src/Client/Account/"+this.username +".db";
            gameData = DriverManager.getConnection(URL);
            Statement statement = gameData.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS oflineGames(gameName TEXT, win INTEGER, lose INTEGER, equal TEXT)");
            if (!isThere("tictactoe"))statement.execute("INSERT INTO oflineGames VALUES ('tictoctoe', 0, 0, 0)");
            gameData.close();

        } catch (SQLException e) {
            System.out.println("Can't connect to save your data!\n Please try again later!");
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    public void showAll(){
        try {
            gameData =DriverManager.getConnection(URL);
            statement = gameData.createStatement();
            statement.execute("SELECT * FROM oflineGames");
            ResultSet result = statement.getResultSet();

            while (result.next()){
                System.out.println("Game name: "+result.getString("gameName")+"\n"+"Times You Won: "
                        +result.getString("win")+"\n"+"Times You Lost: "+result.getString("lose")+"\nTime Draw: "+result.getString("equal"));

             }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                gameData.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addResult(String gameName, int gameResult){
        try {
            gameData = DriverManager.getConnection(URL);

            if (gameResult == 1){

                statement = gameData.createStatement();
                Statement statement = this.gameData.createStatement();
                statement.execute("SELECT win FROM oflineGames");
                ResultSet result = statement.getResultSet();
                int winning = result.getInt(1);
                winning++;
                statement.execute("UPDATE oflineGames SET win="+winning+" WHERE gameName='"+gameName+"'");
                gameData.close();

            }else if (gameResult == -1){

                statement = gameData.createStatement();
                Statement statement = this.gameData.createStatement();
                statement.execute("SELECT lose FROM oflineGames");
                ResultSet result = statement.getResultSet();
                int lose = result.getInt(1);
                lose++;
                statement.execute("UPDATE oflineGames SET lose="+lose+" WHERE gameName='"+gameName+"'");
                gameData.close();

            }else if (gameResult == 0){

                statement = gameData.createStatement();
                Statement statement = this.gameData.createStatement();
                statement.execute("SELECT equal FROM oflineGames");
                ResultSet result = statement.getResultSet();
                int equal = result.getInt(1);
                equal++;
                statement.execute("UPDATE oflineGames SET equal="+equal+" WHERE gameName='"+gameName+"'");

            }
            gameData.close();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public String getEmail() {
        return email;
    }

    public Connection getGameData() {
        return gameData;
    }

    public boolean isThere(String username){
        int result = 0;
        try {
            gameData = DriverManager.getConnection(URL);
            Statement statement = gameData.createStatement();

            statement.execute("SELECT EXISTS(SELECT * FROM oflineGames WHERE gameName = '"+username+"')");
            ResultSet resultSet = statement.getResultSet();
            result = resultSet.getInt(1);
            gameData.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (result == 1)return true;
        else return false;
    }


}
