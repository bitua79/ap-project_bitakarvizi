package Client.Games.TicTacToe_1;

import Client.Controller;

import java.util.Random;

public class Tic_Tac_Toe_Game_1 implements Runnable {

    private int row;
    private int column;

    public Tic_Tac_Toe_Game_1(int row, int column) {
        this.row = row;
        this.column = column;
    }

    @Override
    public void run() {
        //After End Game, Player Not Allowed To Shoot
        if (Board.boardSituation(Board.board) != 0){
            Controller.TicTacToe_1_situation = Board.boardSituation(Board.board);
            return;
        }

        if (Board.board[row][column].getShoot() == ' ' && Board.isTurn){
            Board.board[row][column].setShoot('X');

            Board.isTurn = false;

            //Player Win
            if (Board.boardSituation(Board.board) == 1){
                Controller.TicTacToe_1_situation = 1;
                return;
            }

            try {
                enemyMove();
            }catch (ArrayIndexOutOfBoundsException e){
                return;
            }

        }else return;
    }

    public void enemyMove(){
        Random random = new Random();
        int x;
        int y;

        do {
            int[] turn = MiniMax.findBestMove(Board.board);
            x = turn[0];
            y = turn[1];

        }while (Board.board[x][y].getShoot() != ' ');

        Board.board[x][y].setShoot('O');
        if (Board.boardSituation(Board.board) == 2){
            Controller.TicTacToe_1_situation = 2;
            return;
        }else if (Board.boardSituation(Board.board) == 3){
            Controller.TicTacToe_1_situation = 3;
            return;
        }

        Board.isTurn = true;
    }

}
