package Client.Games.BattleShip;
import Client.Controller;
import javafx.animation.FillTransition;
import javafx.animation.Interpolator;
import javafx.scene.image.Image;
import javafx.scene.media.AudioClip;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Bloc extends Rectangle{

    private int x, y;
    private Piece piece = null;
    private boolean dead = false;
    private Board board;

    //sounds
    private AudioClip kill = new AudioClip("file:src/Client/Media/kill.wav");


    public Bloc(int x, int y, Board board) {
        super(60, 60);
        setStrokeWidth(3);

        setOpacity(0.3);
        setFill(Color.THISTLE);
        setStroke(Paint.valueOf("#949dcd"));

        this.x = x;
        this.y = y;
        this.board = board;
        setOpacity(0.3);
        setOnMouseEntered(event -> {
            if (!isDead()){
                if (board.isEnemy() || (!board.isEnemy() && piece==null))
                    setFill(Color.BLACK);
            }
        });
        setOnMouseExited(event -> {
            if (!isDead()){
                if (board.isEnemy() || (!board.isEnemy() && piece==null))
                    setFill(Color.THISTLE);
            }
        });
    }

    //it set the died cells and return if this cell is a ship cell or not
    public boolean shoot() {
        dead = true;
        if(piece == null){
            setFill(new ImagePattern(new Image(Controller.photo+"GLASS.png")));
            setOpacity(0.8);
        }
        if (board.isEnemy()){
            Controller.clickSound();
        }
        if (piece != null ) {
            piece.hit();
            if (piece.isEnemy()){
                setFill(new ImagePattern(new Image(Controller.photo+"O.png")));
            }else setFill(new ImagePattern(new Image(Controller.photo+"X.png")));

            setOpacity(0.8);
            //after shoot all blocs of a piece
            if (!piece.isAlive()) {
                kill.play();
                //player gets some money after shoot to enemy piece
                if(!piece.isEnemy()){
                    if (piece.getType() == 11){
                        board.kingCount--;
                    }
                    else if (piece.getType() == 10 || piece.getType() == 9) {
                        board.rookCount--;
                    }
                    else if (piece.getType() > 5 && piece.getType() <= 8) {
                        board.knightCount--;
                    }
                    else if (piece.getType() > 0 && piece.getType() <= 5) {
                        board.pawnCount--;
                    }
                }
                board.pieceCount--;
            }
            return true;
        }
        return false;
    }

    //getter and setter
    public Piece getPiece() {
        return piece;
    }
    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean isDead() {
        return dead;
    }
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public int getBlocX() {
        return x;
    }
    public void setBlocX(int x) {
        this.x = x;
    }

    public int getBlocY() {
        return y;
    }
    public void setBlocY(int y) {
        this.y = y;
    }

    public Board getBoard() {
        return board;
    }

}

