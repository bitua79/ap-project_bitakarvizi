package Client.Games.BattleShip;
import java.util.Random;

import Client.Controller;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

public class BattleShipGame_1 extends HBox{
    private Goal goal = null;

    public boolean running = false;
    private int friendsToPlace = 11;
    public boolean enemyTurn = false;
    private Random random = new Random();
    private Board enemyBoard, playerBoard;

    private int x;
    private int y;

    public BattleShipGame_1() {
        enemyBoard = new Board(true, event -> {
            //losing you and winning enemy
            if (playerBoard.pieceCount == 0 || playerBoard.pieceCount == 0) {
                return;
            }

            //after start the game, do this
            if (!running) return;

            //losing you and winning enemy
            if (playerBoard.pieceCount == 0) {
                Controller.BattleShip_1_situation = -1;
            }
            Bloc bloc = (Bloc) event.getSource();
            int x = bloc.getBlocX();
            int y = bloc.getBlocY();
            if (bloc.isDead()) return;

            enemyTurn = !bloc.shoot();

            //loosing enemy and winning you!
            if (enemyBoard.pieceCount == 0) {
                Controller.BattleShip_1_situation = 1;
            }

            if (enemyTurn)
                enemyMove();
        });
        playerBoard = new Board(false, event -> {

            //before start the game, do this
            if (running)
                return;

            Bloc bloc = (Bloc) event.getSource();
            if (playerBoard.placePiece(new Piece(friendsToPlace, event.getButton().equals(MouseButton.PRIMARY), false), bloc.getBlocX(), bloc.getBlocY())) {
                if (--friendsToPlace == 0) {
                    startGame();
                }
            }
        });
        this.getChildren().addAll(enemyBoard, playerBoard);
        this.setSpacing(30);
    }

    private void enemyMove() {
        playerBoard.setDisable(true);
        if (playerBoard.pieceCount == 0) {
            Controller.BattleShip_1_situation = -1;
            enemyTurn = false;
            return;
        }
        while (enemyTurn) {
            //losing you and winning enemy

            if (goal!=null){
                if (goal.getRight()!=null && !goal.getRight().isDead()){
                    x = goal.getRight().getBlocX();
                    y = goal.getRight().getBlocY();

                }else if (goal.getLeft()!=null && !goal.getLeft().isDead()){
                    x = goal.getLeft().getBlocX();
                    y = goal.getLeft().getBlocY();

                }else if (goal.getUp()!=null && !goal.getUp().isDead()) {
                    x = goal.getUp().getBlocX();
                    y = goal.getUp().getBlocY();

                }else if(goal.getDown()!=null && !goal.getDown().isDead()) {
                    x = goal.getDown().getBlocX();
                    y = goal.getDown().getBlocY();

                }else if(goal.getRight_up()!=null && !goal.getRight_up().isDead() && goal.getRight_up().getPiece()!=null){
                    x = goal.getRight_up().getBlocX();
                    y = goal.getRight_up().getBlocY();

                }else if(goal.getRight_down()!=null && !goal.getRight_down().isDead() && goal.getRight_down().getPiece()!=null){
                    x = goal.getRight_down().getBlocX();
                    y = goal.getRight_down().getBlocY();

                }else if(goal.getLeft_up()!=null && !goal.getLeft_up().isDead() && goal.getLeft_up().getPiece()!=null){
                    x = goal.getLeft_up().getBlocX();
                    y = goal.getLeft_up().getBlocY();

                }else if(goal.getLeft_down()!=null && !goal.getLeft_down().isDead() && goal.getLeft_down().getPiece()!=null){
                    x = goal.getLeft_down().getBlocX();
                    y = goal.getLeft_down().getBlocY();

                } else {
                    goal = null;
                    x = random.nextInt(10);
                    y = random.nextInt(10);
                }
            }else {
                x = random.nextInt(10);
                y = random.nextInt(10);
            }

            Bloc bloc = playerBoard.getBloc(x, y);


            if (bloc.isDead())
                continue;

            //in case bloc belongs to a ship, there is another turn
            enemyTurn = bloc.shoot();
            if (enemyTurn){
                goal = new Goal(x, y, playerBoard);
            }

            //losing you and winning enemy
            if (playerBoard.pieceCount == 0) {
                Controller.BattleShip_1_situation = -1;
                return;
            }
        }
        playerBoard.setDisable(false);
    }

    //place enemy ships
    private void startGame() {
        int pieceCount = 11;

        while (pieceCount > 0) {
            int x = random.nextInt(10);
            int y = random.nextInt(10);

            if (enemyBoard.placePiece(new Piece(pieceCount, Math.random() < 0.5, true), x, y)) {
                pieceCount--;
            }
        }
        Timeline placeEnemy = new Timeline(new KeyFrame(new Duration(3000)));
        placeEnemy.setOnFinished(event -> {
            running = true;
        });
        placeEnemy.play();
    }

}