package Client.Games.TicTacToe_2;

import Client.Controller;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Tic_Tac_Toe_Game_2 extends HBox {

    private boolean waiting = true;
    private boolean isTurn = false;
    private boolean continuePlay = true;
    private char myChar = ' ';
    private char enemyChar = ' ';
    private int rowIndex;
    private int columnIndex;
    private DataInputStream input;
    private DataOutputStream output;
    private boolean player1 = false;
    private Label text;
    private int anInt = 0;

    public Tic_Tac_Toe_Game_2(Socket socket, Label text) {
        this.text  = text;

        Board board = new Board(event -> {
            Cell cell = (Cell) event.getSource();
            if (cell.getShoot() ==' ' && isTurn){
                waiting = false;
                isTurn = false;
                columnIndex = cell.getColumn();
                rowIndex = cell.getRow();
                cell.setShoot(myChar);
            }
        });

        getChildren().add(board);

        try {
            playGame(socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playGame(Socket socket) throws IOException {
        input = new DataInputStream(socket.getInputStream());
        output = new DataOutputStream(socket.getOutputStream());


        new Thread(()->{
            try {
                //read a boolean to mention he/she is X/O player
                player1 = input.readBoolean();
                //handle first/second player join and characters
                if (player1){
                    myChar = 'X';
                    enemyChar = 'O';
                    Platform.runLater(()->{
                        text.setText("You joined the game!You are player 1(X) ... Wait for second player to join...");
                    });
                    input.read();
                    Platform.runLater(()->{
                        text.setText("player2 joined the game!Now I should play my turn");
                    });
                    isTurn = true;
                }else {
                    myChar = 'O';
                    enemyChar = 'X';
                    Platform.runLater(()->{
                        text.setText("You joined the game!You are player 2(O) ... Wait for first player to play...");
                    });
                }
                while (continuePlay){
                    //wait for person to choose cell -> send action
                        //snd move to another player -> receive another player action
                    if (player1){
                        waitingFor();
                        sendMove();
                        receiveAction();
                    }else {
                        //receive first player action -> wait for player
                            //to choose cell -> send action and move to another player
                        receiveAction();
                        waitingFor();
                        sendMove();
                    }
                }
            }catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void waitingFor() throws InterruptedException {
        while (waiting){
            Thread.sleep(100);
        }
        waiting = true;
    }

    public void sendMove() throws IOException {
        output.writeInt(rowIndex);
        output.writeInt(columnIndex);
    }

    //receive a number which mention game situation
    public void receiveAction() throws IOException {
        int result = input.readInt();

        //if 0 -> continue the game and change the turn
        if (result==0){
            receiveMove();
            Platform.runLater(() -> {
                text.setText("This is MyTurn  "+ myChar);
            });
            isTurn = true;
            return;
        }

        //stop game and mention winner
        continuePlay = false;

        //1 -> X win
        if (result == 1) {
            if (myChar == 'X') {
                Platform.runLater(() -> {
                    text.setText("I won (X)");

                });
            } else if (myChar == 'O') {
                Platform.runLater(() -> {
                    text.setText("player2 won (X)");
                });

                receiveMove();
            }
        }

        //2 ->  Y win
        else if (result == 2){
            if (myChar == 'O'){
                Platform.runLater(()->{
                    text.setText("I won (O)");
                });

            }else if (myChar == 'X'){
                Platform.runLater(()->{
                    text.setText("player2 won (O)");
                });

                receiveMove();
            }
        }

        //3 -> it is a draw
        else if(result == 3){
            Platform.runLater(()->{
                text.setText("game has been draw!");
            });

            //because only player1 can move in last turn in a draw game(turn counts is odd)
            if (myChar == 'O') {
                receiveMove();
            }
        }
    }

    //set another player action to this player board too
    public void receiveMove() throws IOException {
        rowIndex = input.readInt();
        columnIndex = input.readInt();
        Platform.runLater(()->{
            Board.cell[rowIndex][columnIndex].setShoot(enemyChar);
        });
    }

    public int situation(){
        return anInt;
    }
}
