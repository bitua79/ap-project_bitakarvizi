package NetworkingTicTacToe.Client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import javax.crypto.spec.PSource;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client extends Application {
    private boolean waiting = true;
    private boolean isTurn = false;
    private boolean continuePlay = true;
    private char myChar = ' ';
    private char enemyChar = ' ';
    private int rowIndex;
    private int columnIndex;
    private DataInputStream input;
    private DataOutputStream output;
    private Cell[][] cell = new Cell[3][3];
    private boolean player1 = false;

    private Label text= new Label("Starting . . .");

    @Override
    public void start(Stage stage) throws Exception{
        stage.setScene(new Scene(new HBox(hBox(), text),
                600, 300));
        stage.show();
        Socket socket = new Socket("localhost", 8000);
        playGame(socket);
    }
    public void playGame(Socket socke) throws IOException {
        input = new DataInputStream(socke.getInputStream());
        output = new DataOutputStream(socke.getOutputStream());


        new Thread(()->{
            try {
                player1 = input.readBoolean();
                System.out.println(player1);
                if (player1){
                    myChar = 'X';
                    enemyChar = 'Y';
                    Platform.runLater(()->{
                        text.setText("You joined the game!You are player 1(X)\n Wait for second player to join...");
                    });
                    input.read();
                    Platform.runLater(()->{
                        text.setText("player2 joined the game!Now I should play my turn");
                    });
                    isTurn = true;
                }else {
                    myChar = 'Y';
                    enemyChar = 'X';
                    Platform.runLater(()->{
                        text.setText("You joined the game!You are player 2(Y)\n Wait for first player to play...");
                    });
                }
                System.out.println("here1!");
                while (continuePlay){

                    if (player1){
                        waitingFor();
                        sendMove();
                        recieveAction();
                    }else {
                        recieveAction();
                        waitingFor();
                        sendMove();
                    }
                }
            }catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public HBox hBox(){
        HBox all = new HBox();
        VBox rows = new VBox();
        for (int y = 0; y < 3; y++) {
            HBox row = new HBox();
            for (int x = 0; x < 3; x++) {
                cell[x][y] = new Cell(x, y);
                row.getChildren().add(cell[x][y]);
            }
            rows.getChildren().add(row);
        }
        all.getChildren().add(rows);
        return all;
    }

    public class Cell extends Rectangle{
        private char shoot = ' ';
        public Cell(int row, int column) {
            super(100, 100);
            this.setOnMouseClicked(e ->{
                if (shoot==' ' && isTurn){
                    waiting = false;
                    isTurn = false;
                    columnIndex = column;
                    rowIndex = row;
                    setShoot(myChar);
                }
            });
        }

        public char getShoot() {
            return shoot;
        }

        public void setShoot(char shoot) {
            this.shoot = shoot;
            if (shoot == 'X'){
                this.setFill(Color.RED);
            }else if(shoot == 'Y'){
                this.setFill(Color.BLUE);
            }
        }
    }

    public void waitingFor() throws InterruptedException {
        while (waiting){
            Thread.sleep(100);
        }
        waiting = true;
    }

    public void sendMove() throws IOException {
        output.writeInt(rowIndex);
        output.writeInt(columnIndex);
    }

    public void recieveAction() throws IOException {
        int result = input.readInt();
        if (result==0){
            recieveMove();
            Platform.runLater(() -> {
                text.setText("This is MyTurn  "+ myChar);
            });
            isTurn = true;
            return;
        }
        continuePlay = false;
        if (result == 1) {
            if (myChar == 'X') {
                Platform.runLater(() -> {
                    text.setText("I won (X)");
                });
            } else if (myChar == 'Y') {
                Platform.runLater(() -> {
                    text.setText("player2 won (X)");
                });
                recieveMove();
            }
        }
        else if (result == 2){
            if (myChar == 'Y'){
                Platform.runLater(()->{
                    text.setText("I won (Y)");
                });
            }else if (myChar == 'X'){
                Platform.runLater(()->{
                    text.setText("player2 won (Y)");
                });
                recieveMove();
            }
        }
        else if(result == 3){
            Platform.runLater(()->{
                text.setText("game has been draw!");
            });
            //because only player1 can move in last turn in a draw game(turn counts is odd)
            if (myChar == 'O') {
                recieveMove();
            }
        }
    }

    public void recieveMove() throws IOException {
        rowIndex = input.readInt();
        columnIndex = input.readInt();
        Platform.runLater(()->{
            cell[rowIndex][columnIndex].setShoot(enemyChar);
        });
    }
}
