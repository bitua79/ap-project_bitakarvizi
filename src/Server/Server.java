package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.Map;
import java.util.Set;

public class Server {

    public static String URL = "jdbc:sqlite:src/Server/users.db";
    public static Connection users;

    public static void main(String[] args) {
        Statement statement;
        try {
            //create a data to save users and their information
            users = DriverManager.getConnection(URL);
            statement = users.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS USERS(username TEXT, password TEXT, email TEXT,question TEXT,answer TEXT)");
            users.close();

            ServerSocket retrievalServSo = new ServerSocket(7000);
            ServerSocket signUpServSo = new ServerSocket(8000);
            ServerSocket signInServSo = new ServerSocket(9000);

            ServerSocket tictavtoeGameServSo = new ServerSocket(10000);
            ServerSocket tictactoeChatServSo = new ServerSocket(10500);

            //threads to handle every part

            // -> retrieval password
            new Thread(() -> {
                try {
                    //to handle lots of clients which connect any time
                    while (true) {

                        System.out.println("Waiting for retrieval socket . . .");
                        Socket retrievalSo = retrievalServSo.accept();
                        System.out.println("retrieval socket connected.");
                        new Thread(new retrievalHandle(retrievalSo)).start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            // -> sign up
            new Thread(() -> {
                try {
                    //to handle lots of clients which connect any time
                    while (true) {

                        System.out.println("Waiting for sign up socket . . .");
                        Socket signUpSo = signUpServSo.accept();
                        System.out.println("sign up socket connected.");
                        new Thread(new signUpHandle(signUpSo)).start();

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }).start();

            // -> sign in
            new Thread(() -> {
                try {
                    //to handle lots of clients which connect any time
                    while (true) {

                        System.out.println("Waiting for sign in socket . . .");
                        Socket signInSo = signInServSo.accept();
                        System.out.println("sign in socket connected.");
                        new Thread(new signInHandle(signInSo)).start();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            // -> tic tac toe game
            new Thread(() -> {
                try {
                    //to handle lots of clients which connect any time
                    while (true) {

                        System.out.println("Waiting for tictacto game first player socket . . .");
                        Socket player1So = tictavtoeGameServSo.accept();
                        //confirm to player1 he/she is first player
                        new DataOutputStream(player1So.getOutputStream()).writeBoolean(true);

                        System.out.println("Waiting for tictacto game second player socket . . .");
                        Socket player2So = tictavtoeGameServSo.accept();
                        new DataOutputStream(player2So.getOutputStream()).writeBoolean(false);

                        System.out.println("tictacto game players sockets connected.");
                        new Thread(new PlatTic(player1So, player2So)).start();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            // -> chat
            new Thread(() -> {
                try {
                    //to handle lots of clients which connect any time
                    while (true) {

                        System.out.println("waiting for people...");

                        Socket socket1 = tictactoeChatServSo.accept();
                        System.out.println("Person1 Joined!");

                        Socket socket2 = tictactoeChatServSo.accept();
                        System.out.println("Person2 Joined!");

                        new Thread(new Chat(socket1, socket2)).start();
                        new Thread(new Chat(socket2, socket1)).start();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    //get username and password from client and return:
    //-1 -> if username doesn't exist
    // 0 -> if question and answer are not match
    // 1 -> if question and answer match
    static class retrievalHandle implements Runnable {
        Socket socket;

        public retrievalHandle(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {

                DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
                DataInputStream fromClient = new DataInputStream(socket.getInputStream());

                String username = fromClient.readUTF();
                String question = fromClient.readUTF();
                String answer = fromClient.readUTF();

                System.out.println("User Recovery Password: " + username + "   " + question + "   " + answer);
                if (!isThere(username)) toClient.writeUTF("-1");
                else if (!answer.equals(getAnswer(username)) || !question.equals(getQuestion(username)))
                    toClient.writeUTF("0");
                else if (answer.equals(getAnswer(username)) && question.equals(getQuestion(username))) {
                    toClient.writeUTF("1");
                    toClient.writeUTF(getPass(username));
                }

                System.out.println();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //get information from client and return:
    //-1 -> if username exist
    // 1 -> if user saved successfully
    static class signUpHandle implements Runnable {
        Socket socket;

        public signUpHandle(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
                DataInputStream fromClient = new DataInputStream(socket.getInputStream());

                String username = fromClient.readUTF();
                String password = fromClient.readUTF();
                String email = fromClient.readUTF();
                String question = fromClient.readUTF();
                String answer = fromClient.readUTF();

                System.out.println("New User: " + username + "   " + password + "   " + email + "   " + question + "   " + answer);

                if (isThere(username)) toClient.writeUTF("-1");
                else {
                    addUser(username, password, email, question, answer);
                    toClient.writeUTF("1");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //get username and password from client and return:
    //-1 -> if username doesn't exist
    // 0 -> if username and password are not match
    // 1 -> if username and password match
    static class signInHandle implements Runnable {
        Socket socket;

        public signInHandle(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
                DataInputStream fromClient = new DataInputStream(socket.getInputStream());

                String username = fromClient.readUTF();
                String password = fromClient.readUTF();

                System.out.println("Online User: " + username + "   " + password + "   ");

                if (!isThere(username)) toClient.writeUTF("-1");
                else if (!password.equals(getPass(username))) toClient.writeUTF("0");
                else if (password.equals(getPass(username))) {
                    toClient.writeUTF("1");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //get password string of the user
    public static String getPass(String username) {
        String result = null;
        try {
            users = DriverManager.getConnection(URL);
            Statement statement = users.createStatement();
            statement.execute("SELECT password FROM USERS WHERE username = '" + username + "'");
            ResultSet resultSet = statement.getResultSet();
            result = resultSet.getString(1);
            users.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    //get question string of the user
    public static String getQuestion(String username) {
        String result = null;
        try {
            users = DriverManager.getConnection(URL);
            Statement statement = users.createStatement();
            statement.execute("SELECT question FROM USERS WHERE username = '" + username + "'");
            ResultSet resultSet = statement.getResultSet();
            result = resultSet.getString(1);
            users.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    //get answer string of the user
    public static String getAnswer(String username) {
        String result = null;
        try {
            users = DriverManager.getConnection(URL);
            Statement statement = users.createStatement();
            statement.execute("SELECT answer FROM USERS WHERE username = '" + username + "'");
            ResultSet resultSet = statement.getResultSet();
            result = resultSet.getString(1);
            users.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    //check the username exist or not
    public static boolean isThere(String username) {
        int result = 0;
        try {
            users = DriverManager.getConnection(URL);
            Statement statement = users.createStatement();

            statement.execute("SELECT EXISTS(SELECT * FROM USERS WHERE username = '" + username + "')");
            ResultSet resultSet = statement.getResultSet();
            result = resultSet.getInt(1);
            users.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (result == 1) return true;
        else return false;
    }

    //add new user to users data
    public static void addUser(String username, String password, String email, String question, String answer) {
        try {
            users = DriverManager.getConnection(URL);
            Statement statement = users.createStatement();
            statement.execute("INSERT INTO USERS VALUES ('" + username + "', '" + password + "', '" + email + "', '" + question + "', '" + answer + "')");
            users.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static class Chat implements Runnable{
        Socket socket1;
        Socket socket2;

        public Chat(Socket socket1, Socket socket2) {
            this.socket1 = socket1;
            this.socket2 = socket2;
        }

        @Override
        public void run() {
            try {
                DataInputStream from1 = new DataInputStream(socket1.getInputStream());
                DataOutputStream to1 = new DataOutputStream(socket1.getOutputStream());
                DataOutputStream to2 = new DataOutputStream(socket2.getOutputStream());
                while (true){
                    String massage = from1.readUTF();
                    to1.writeUTF(" You said: "+massage);
                    to2.writeUTF(" Your friend said: "+massage);
                    System.out.println(massage +" sent.");
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
