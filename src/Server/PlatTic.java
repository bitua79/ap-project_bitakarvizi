package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class PlatTic implements Runnable{
    public static char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
    private Socket player1;
    private Socket player2;

    public PlatTic(Socket player1, Socket player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    private DataInputStream inputStreamPlayer1;
    private DataOutputStream outputStreamPlayer1;

    private DataInputStream inputStreamPlayer2;
    private DataOutputStream outputStreamPlayer2;

    @Override
    public void run() {
     try {
        inputStreamPlayer1 = new DataInputStream(player1.getInputStream());
        outputStreamPlayer1 = new DataOutputStream(player1.getOutputStream());
        inputStreamPlayer2 = new DataInputStream(player2.getInputStream());
        outputStreamPlayer2 = new DataOutputStream(player2.getOutputStream());

        outputStreamPlayer1.writeBoolean(true);

        while (true) {

            int row = inputStreamPlayer1.readInt();
            int column = inputStreamPlayer1.readInt();
            board[row][column] = 'X';

            // Check if Player 1 wins
            if (checkPlayGround()==1) {
                outputStreamPlayer1.writeInt(1);
                outputStreamPlayer2.writeInt(1);
                sendMove(outputStreamPlayer2, row, column);
                break;
            }
            else if (checkPlayGround()==3) {
                // Check if all cells are filled
                outputStreamPlayer1.writeInt(3);
                outputStreamPlayer2.writeInt(3);
                sendMove(outputStreamPlayer2, row, column);
                break;
            }
            else if (checkPlayGround()==0){
                outputStreamPlayer2.writeInt(0);
                sendMove(outputStreamPlayer2, row, column);
            }

            // Receive a move from Player 2
            row = inputStreamPlayer2.readInt();
            column = inputStreamPlayer2.readInt();
            board[row][column] = 'O';

            // Check if Player 2 wins
            if (checkPlayGround()==2) {
                outputStreamPlayer1.writeInt(2);
                outputStreamPlayer2.writeInt(2);
                sendMove(outputStreamPlayer1, row, column);
                break;
            }
            else if (checkPlayGround()==0){
                outputStreamPlayer1.writeInt(0);
                sendMove(outputStreamPlayer1, row, column);
            }

            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void sendMove(DataOutputStream output, int row, int column) throws IOException {
        output.writeInt(row);
        output.writeInt(column);
    }

    public static int checkPlayGround() {
        // returns 0 if game is not finished
        // returns 1 if X wins
        // returns 2 if O wins
        // returns 3 if game has been draw
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == 'X' && board[i][1] == 'X' && board[i][2] == 'X') return 1;
            if (board[i][0] == 'O' && board[i][1] == 'O' && board[i][2] == 'O') return 2;
        }
        for (int i = 0; i < 3; i++) {
            if (board[0][i] == 'X' && board[1][i] == 'X' && board[2][i] == 'X') return 1;
            if (board[0][i] == 'O' && board[1][i] == 'O' && board[2][i] == 'O') return 2;
        }
        if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X') return 1;
        if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') return 1;

        if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O') return 2;
        if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') return 2;

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (board[i][j] == ' ') return 0;

        return 3;
    }

}
